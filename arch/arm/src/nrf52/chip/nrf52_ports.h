/************************************************************************************
 * arch/arm/src/nrf52/chip/nrf52_ports.h
 *
 *   Copyright (C) 2017 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Reference: NRF52500 Reference Manual V1.5 2014-07 Microcontrollers.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * May include some logic from sample code provided by Infineon:
 *
 *   Copyright (C) 2011-2015 Infineon Technologies AG. All rights reserved.
 *
 * Infineon Technologies AG (Infineon) is supplying this software for use with
 * Infineon's microcontrollers.  This file can be freely distributed within
 * development tools that are supporting such microcontrollers.
 *
 * THIS SOFTWARE IS PROVIDED AS IS. NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 ************************************************************************************/

#ifndef __ARCH_ARM_SRC_NRF52_CHIP_NRF52_PORTS_H
#define __ARCH_ARM_SRC_NRF52_CHIP_NRF52_PORTS_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include "chip/nrf52_memorymap.h"

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Register Offsets *****************************************************************/

/* GPIO Registers */

#define NRF52_PORT_OUT        ( NRF52_GPIO_BASE + 0x0504 )    /* Write GPIO port */
#define NRF52_PORT_OUTSET     ( NRF52_GPIO_BASE + 0x0508 )    /* Set individual bits in GPIO Port */
#define NRF52_PORT_OUTCLR     ( NRF52_GPIO_BASE + 0x050c )    /* Clear individual bits in GPIO port */
#define NRF52_PORT_IN         ( NRF52_GPIO_BASE + 0x0510 )    /* Read GPIO port */
#define NRF52_PORT_DIR        ( NRF52_GPIO_BASE + 0x0514 )    /* Direction of GPIO pins */
#define NRF52_PORT_DIRSET     ( NRF52_GPIO_BASE + 0x0518 )    /* DIR set register */
#define NRF52_PORT_DIRCLR     ( NRF52_GPIO_BASE + 0x051c )    /* DIR clear register */
#define NRF52_PORT_LATCH      ( NRF52_GPIO_BASE + 0x0520 )    /* Indicate what GPIO pins that have met the criteria */
#define NRF52_PORT_DETECTMODE ( NRF52_GPIO_BASE + 0x0524 )    /* Select DETECT mode */

/* Pin Configuration Registers */
#define NRF52_PIN_CNF_0 	0x700
#define NRF52_PIN_CNF_1 	0x704
#define NRF52_PIN_CNF_2 	0x708
#define NRF52_PIN_CNF_3 	0x70c
#define NRF52_PIN_CNF_4 	0x710
#define NRF52_PIN_CNF_5 	0x714
#define NRF52_PIN_CNF_6 	0x718
#define NRF52_PIN_CNF_7 	0x71c
#define NRF52_PIN_CNF_8 	0x720
#define NRF52_PIN_CNF_9 	0x724
#define NRF52_PIN_CNF_10 	0x728
#define NRF52_PIN_CNF_11 	0x72c
#define NRF52_PIN_CNF_12 	0x730
#define NRF52_PIN_CNF_13 	0x734
#define NRF52_PIN_CNF_14 	0x738
#define NRF52_PIN_CNF_15 	0x73c
#define NRF52_PIN_CNF_16 	0x740
#define NRF52_PIN_CNF_17 	0x744
#define NRF52_PIN_CNF_18 	0x748
#define NRF52_PIN_CNF_19 	0x74c
#define NRF52_PIN_CNF_20 	0x750
#define NRF52_PIN_CNF_21 	0x754
#define NRF52_PIN_CNF_22 	0x758
#define NRF52_PIN_CNF_23 	0x75c
#define NRF52_PIN_CNF_24 	0x760
#define NRF52_PIN_CNF_25 	0x764
#define NRF52_PIN_CNF_26 	0x768
#define NRF52_PIN_CNF_27 	0x76c
#define NRF52_PIN_CNF_28 	0x770
#define NRF52_PIN_CNF_29 	0x774
#define NRF52_PIN_CNF_30 	0x778
#define NRF52_PIN_CNF_31 	0x77c


/* Register Addresses **************************************************************

#define NRF52_PORT0_OUT              (NRF52_PORT0_BASE+NRF52_PORT_OUT_OFFSET)
#define NRF52_PORT0_OMR              (NRF52_PORT0_BASE+NRF52_PORT_OMR_OFFSET)
#define NRF52_PORT0_IOCR0            (NRF52_PORT0_BASE+NRF52_PORT_IOCR0_OFFSET)
#define NRF52_PORT0_IOCR4            (NRF52_PORT0_BASE+NRF52_PORT_IOCR4_OFFSET)
#define NRF52_PORT0_IOCR8            (NRF52_PORT0_BASE+NRF52_PORT_IOCR8_OFFSET)
#define NRF52_PORT0_IOCR12           (NRF52_PORT0_BASE+NRF52_PORT_IOCR12_OFFSET)
#define NRF52_PORT0_IN               (NRF52_PORT0_BASE+NRF52_PORT_IN_OFFSET)
#define NRF52_PORT0_PDR0             (NRF52_PORT0_BASE+NRF52_PORT_PDR0_OFFSET)
#define NRF52_PORT0_PDR1             (NRF52_PORT0_BASE+NRF52_PORT_PDR1_OFFSET)
#define NRF52_PORT0_PDISC            (NRF52_PORT0_BASE+NRF52_PORT_PDISC_OFFSET)
#define NRF52_PORT0_PPS              (NRF52_PORT0_BASE+NRF52_PORT_PPS_OFFSET)
#define NRF52_PORT0_HWSEL            (NRF52_PORT0_BASE+NRF52_PORT_HWSEL_OFFSET)
*/


/* Register Bit-Field Definitions **************************************************/


/* Register GPIO Pin Configurations (PIN_CNF[x]) */
#define GPIO_PIN_CNF_DIR_SHIFT		0
#define GPIO_PIN_CNF_DIR_INPUT		( 0 << PIN_CNF_DIR_SHIFT)
#define GPIO_PIN_CNF_DIR_OUTPUT		( 1 << PIN_CNF_DIR_SHIFT)

#define GPIO_PIN_CNF_INPUT_SHIFT	1
#define GPIO_PIN_CNF_INPUT_CONNECT	( 0 << PIN_CNF_INPUT_SHIFT )
#define GPIO_PIN_CNF_INPUT_DISCONNECT	( 1 << PIN_CNF_INPUT_SHIFT )

#define GPIO_PIN_CNF_PULL_SHIFT		2
#define GPIO_PIN_CNF_PULL_DISABLED	( 0 << GPIO_PIN_CNF_PULL_SHIFT )
#define GPIO_PIN_CNF_PULL_PULLDOWN	( 1 << GPIO_PIN_CNF_PULL_SHIFT )
#define GPIO_PIN_CNF_PULL_PULLUP	( 3 << GPIO_PIN_CNF_PULL_SHIFT )
#define GPIO_PIN_CNF_PULL_MASK		( 3 << GPIO_PIN_CNF_PULL_SHIFT )

#define GPIO_PIN_CNF_DRIVE_SHIFT	0x8
#define GPIO_PIN_CNF_DRIVE_S0S1		( 0 << GPIO_PIN_CNF_DRIVE_SHIFT )
#define GPIO_PIN_CNF_DRIVE_H0S1		( 1 << GPIO_PIN_CNF_DRIVE_SHIFT )
#define GPIO_PIN_CNF_DRIVE_S0H1		( 2 << GPIO_PIN_CNF_DRIVE_SHIFT )
#define GPIO_PIN_CNF_DRIVE_H0H1		( 3 << GPIO_PIN_CNF_DRIVE_SHIFT )
#define GPIO_PIN_CNF_DRIVE_D0S1		( 4 << GPIO_PIN_CNF_DRIVE_SHIFT )
#define GPIO_PIN_CNF_DRIVE_D0H1		( 5 << GPIO_PIN_CNF_DRIVE_SHIFT )
#define GPIO_PIN_CNF_DRIVE_S0D1		( 6 << GPIO_PIN_CNF_DRIVE_SHIFT )
#define GPIO_PIN_CNF_DRIVE_H0D1		( 7 << GPIO_PIN_CNF_DRIVE_SHIFT )
#define GPIO_PIN_CNF_DRIVE_MASK		( 7 << GPIO_PIN_CNF_DRIVE_SHIFT )

#define GPIO_PIN_CNF_SENSE_SHIFT	16
#define GPIO_PIN_CNF_SENSE_DISABLED	( 0 << GPIO_PIN_CNF_SENSE_SHIFT )
#define GPIO_PIN_CNF_SENSE_HIGH		( 2 << GPIO_PIN_CNF_SENSE_SHIFT )
#define GPIO_PIN_CNF_SENSE_DOWN		( 3 << GPIO_PIN_CNF_SENSE_SHIFT )
#define GPIO_PIN_CNF_SENSE_MASK		( 3 << GPIO_PIN_CNF_SENSE_SHIFT )


#endif /* __ARCH_ARM_SRC_NRF52_CHIP_NRF52_PORTS_H */
